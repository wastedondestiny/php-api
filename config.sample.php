<?php

return [
    'apiKey' => '',
    'bugsnagApiKey' => '',
    'cache' => [
        'provider' => WastedOnDestiny\Business\Cache\RedisCacheStorage::class,
        'uri' => 'tcp://127.0.0.1:6379',
        'ttl' => 3600
    ],
    'internalCache' => [
        'provider' => WastedOnDestiny\Business\Cache\RedisCacheStorage::class,
        'uri' => 'tcp://127.0.0.1:6379',
        'ttl' => 3600
    ],
    'database' => [
        'type' => 'mysql',
        'host' => 'localhost',
        'port' => '3306',
        'user' => 'root',
        'password' => '',
        'name' => 'wastedondestiny',
        'charset' => 'utf8mb4'
    ]
];
