<?php 
class BusinessFactoryGetDestinyPlayersTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGivenValidBungieNetPlatformResponseWhenGetDestinyPlayersThenDestinyPlayerArray()
    {
        $data = '[{"membershipType":4,"membershipId":"12345678901234567890",
                "displayName":"TestUser#9999"}]';
        $response = new \WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse();
        $response->response = json_decode($data);
        $factory = new \WastedOnDestiny\DataAccess\BusinessFactory();
        $result = $factory->getDestinyPlayers($response);

        $this->assertNotEmpty($result);
        $this->assertCount(1, $result);
        $this->assertInstanceOf(\WastedOnDestiny\Business\Models\DestinyPlayer::class, $result[0]);
        $this->assertEquals('12345678901234567890', $result[0]->membershipId);
        $this->assertEquals(4, $result[0]->membershipType);
    }

    public function testGivenNullWhenGetDestinyPlayersThenInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);

        $data = null;
        $response = new \WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse();
        $response->response = json_decode($data);
        $factory = new \WastedOnDestiny\DataAccess\BusinessFactory();
        $factory->getDestinyPlayers($response);
    }
}