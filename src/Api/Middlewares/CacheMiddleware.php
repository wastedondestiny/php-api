<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Middlewares;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class CacheMiddleware
{
    protected $cache;

    public function __construct(ContainerInterface $container)
    {
        $this->cache = $container->get(\WastedOnDestiny\Business\Cache\CacheStorageInterface::class);
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $key = str_replace('/', '_', $request->getUri()->getPath()) . sha1($request->getUri()->getQuery());
        $existing = $this->cache->fetch($key);

        if (!$existing) {
            /** @var ResponseInterface $response */
            $response = $next($request, $response);
            $this->cache->save($key, (string)$response->getBody());
            return $response;
        }

        $body = $response->getBody();
        $body->write($existing);
        $response = $response->withBody($body);

        return $response;
    }
}
