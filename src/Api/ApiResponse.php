<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api;

use DateTime;

class ApiResponse
{
    /** @var string */
    public $version = '2.0';
    
    /** @var int */
    public $code = 200;
    
    /** @var mixed */
    public $response;
    
    /** @var string */
    public $message = 'Ok';
    
    /** @var \DateTime */
    public $queryTime;
    
    /** @var float */
    public $executionMilliseconds;

    public function __construct()
    {
        $this->queryTime = new DateTime('now');
        $this->executionMilliseconds = microtime(true);
    }

    /**
     * @param mixed|null $response
     * @return ApiResponse
     */
    public function get($response = null): ApiResponse
    {
        $this->response = $response;
        $this->executionMilliseconds = (microtime(true) - $this->executionMilliseconds) * 1000;
        return $this;
    }
}