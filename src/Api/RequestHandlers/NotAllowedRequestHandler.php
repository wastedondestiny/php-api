<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\RequestHandlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;

class NotAllowedRequestHandler
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $methods)
    {
        $apiResponse = (new ApiResponse())->get([
            'allowedMethods' => $methods
        ]);
        $apiResponse->code = 405;
        $apiResponse->message = 'Not Allowed';
        $body = $response->getBody();
        $body->write(json_encode($apiResponse));
        return $response
            ->withStatus(405)
            ->withHeader('content-type', 'application/json')
            ->withBody($body);
    }
}
