<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Controllers;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;
use WastedOnDestiny\Application\UseCases\GetCharactersUseCaseInterface;

class CharactersController
{
    /** @var GetCharactersUseCaseInterface */
    private $useCase;

    public function __construct(GetCharactersUseCaseInterface $getCharactersUseCase)
    {
        $this->useCase = $getCharactersUseCase;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $gameVersion = $request->getQueryParams()['gameVersion'] ?? null;
        $membershipType = $request->getQueryParams()['membershipType'] ?? null;
        $membershipId = $request->getQueryParams()['membershipId'] ?? null;

        if (empty($gameVersion) || empty($membershipType) || empty($membershipId)) {
            throw new InvalidArgumentException('Usage: "gameVersion" (required), "membershipId" (required) and "membershipType" (required)');
        }

        $body = $response->getBody();
        $body->write(json_encode((new ApiResponse())->get($this->useCase->execute((int)$gameVersion, (int)$membershipType, (string)$membershipId))));
        return $response->withBody($body);
    }
}
