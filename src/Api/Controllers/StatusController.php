<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;
use WastedOnDestiny\Application\UseCases\GetGlobalAlertsUseCaseInterface;

class StatusController
{
    /** @var GetGlobalAlertsUseCaseInterface */
    private $useCase;

    public function __construct(GetGlobalAlertsUseCaseInterface $getGlobalAlertsUseCase)
    {
        $this->useCase = $getGlobalAlertsUseCase;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $body = $response->getBody();
        $body->write(json_encode((new ApiResponse())->get($this->useCase->execute())));
        return $response->withBody($body);
    }
}
