<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Models;

class ActivityBreakdown
{
    /** @var int */
    public $story = 0;

    /** @var int */
    public $strike = 0;

    /** @var int */
    public $raid = 0;

    /** @var int */
    public $gambit = 0;

    /** @var int */
    public $crucible = 0;
}
