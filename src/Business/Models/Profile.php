<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Models;

use DateTime;

class Profile
{
    /** @var DateTime */
    public $dateLastPlayed;

    /** @var int */
    public $triumph;

    /** @var Destiny2CharacterData[] */
    public $characters;

    /** @var bool */
    public $crossSaveActive;

    /** @var string */
    public $displayName;

    /** @var CharacterActivities[] */
    public $characterActivities = [];

    /** @var int */
    public $versionOwned;

    /** @var int[] */
    public $seasonsOwned;
}
