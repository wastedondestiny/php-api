<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Models;

class CharacterActivities
{
    /** @var int */
    public $characterId;

    /** @var int */
    public $currentActivityModeType;

    /** @var string */
    public $currentActivityHash;

    /** @var string */
    public $currentActivityModeHash;

    /** @var array */
    public $activities;
}
