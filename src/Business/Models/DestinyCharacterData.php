<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Models;

class DestinyCharacterData
{
    /** @var string */
    public $backgroundPath;

    /** @var string */
    public $characterId;

    /** @var string */
    public $charClass;

    /** @var string */
    public $emblemPath;

    /** @var string */
    public $gender;

    /** @var int */
    public $level;

    /** @var int */
    public $light;

    /** @var string */
    public $race;
}
