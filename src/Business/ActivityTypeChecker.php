<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business;

use WastedOnDestiny\Business\Enums\Destiny2ActivityModeType;

class ActivityTypeChecker
{
    public function isStory(int $activityMode): bool
    {
        return in_array($activityMode, [
            Destiny2ActivityModeType::Story,
            Destiny2ActivityModeType::Patrol,
            Destiny2ActivityModeType::HeroicAdventure,
            Destiny2ActivityModeType::BlackArmoryRun,
            Destiny2ActivityModeType::Menagerie,
            Destiny2ActivityModeType::VexOffensive,
            Destiny2ActivityModeType::Dungeon,
            Destiny2ActivityModeType::Sundial
        ], true);
    }

    public function isStrike(int $activityMode): bool
    {
        return in_array($activityMode, [
            Destiny2ActivityModeType::Strike,
            Destiny2ActivityModeType::Nightfall,
            Destiny2ActivityModeType::HeroicNightfall,
            Destiny2ActivityModeType::AllStrikes,
            Destiny2ActivityModeType::ScoredNightfall,
            Destiny2ActivityModeType::ScoredHeroicNightfall,
            Destiny2ActivityModeType::NightmareHunt
        ], true);
    }

    public function isRaid(int $activityMode): bool
    {
        return $activityMode === Destiny2ActivityModeType::Raid;
    }

    public function isGambit(int $activityMode): bool
    {
        return in_array($activityMode, [
            Destiny2ActivityModeType::Gambit,
            Destiny2ActivityModeType::AllPvECompetitive,
            Destiny2ActivityModeType::GambitPrime,
            Destiny2ActivityModeType::Reckoning
        ], true);
    }

    public function isCrucible(int $activityMode): bool
    {
        return in_array($activityMode, [
            Destiny2ActivityModeType::AllPvP,
            Destiny2ActivityModeType::Control,
            Destiny2ActivityModeType::Clash,
            Destiny2ActivityModeType::CrimsonDoubles,
            Destiny2ActivityModeType::IronBanner,
            Destiny2ActivityModeType::AllMayhem,
            Destiny2ActivityModeType::Supremacy,
            Destiny2ActivityModeType::PrivateMatchesAll,
            Destiny2ActivityModeType::Survival,
            Destiny2ActivityModeType::Countdown,
            Destiny2ActivityModeType::TrialsOfTheNine,
            Destiny2ActivityModeType::TrialsCountdown,
            Destiny2ActivityModeType::TrialsSurvival,
            Destiny2ActivityModeType::IronBannerControl,
            Destiny2ActivityModeType::IronBannerClash,
            Destiny2ActivityModeType::IronBannerSupremacy,
            Destiny2ActivityModeType::Rumble,
            Destiny2ActivityModeType::AllDoubles,
            Destiny2ActivityModeType::Doubles,
            Destiny2ActivityModeType::PrivateMatchesClash,
            Destiny2ActivityModeType::PrivateMatchesControl,
            Destiny2ActivityModeType::PrivateMatchesSupremacy,
            Destiny2ActivityModeType::PrivateMatchesCountdown,
            Destiny2ActivityModeType::PrivateMatchesSurvival,
            Destiny2ActivityModeType::PrivateMatchesMayhem,
            Destiny2ActivityModeType::PrivateMatchesRumble,
            Destiny2ActivityModeType::Showdown,
            Destiny2ActivityModeType::Lockdown,
            Destiny2ActivityModeType::Scorched,
            Destiny2ActivityModeType::ScorchedTeam,
            Destiny2ActivityModeType::Breakthrough,
            Destiny2ActivityModeType::Salvage,
            Destiny2ActivityModeType::IronBannerSalvage,
            Destiny2ActivityModeType::PvPCompetitive,
            Destiny2ActivityModeType::PvPQuickplay,
            Destiny2ActivityModeType::ClashQuickplay,
            Destiny2ActivityModeType::ClashCompetitive,
            Destiny2ActivityModeType::ControlQuickplay,
            Destiny2ActivityModeType::ControlCompetitive,
            Destiny2ActivityModeType::Elimination,
            Destiny2ActivityModeType::Momentum,
            Destiny2ActivityModeType::TrialsOfOsiris
        ], true);
    }
}
