<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Application\Models\LeaderboardPage;
use WastedOnDestiny\Business\Repositories\Leaderboard\LeaderboardRepositoryInterface;

class GetLeaderboardPageUseCase implements GetLeaderboardPageUseCaseInterface
{
    /** @var LeaderboardRepositoryInterface */
    private $leaderboardRepository;
    /** @var ModelFactory */
    private $factory;

    public function __construct(LeaderboardRepositoryInterface $leaderboardRepository)
    {
        $this->leaderboardRepository = $leaderboardRepository;
        $this->factory = new ModelFactory();
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param int $page
     * @return LeaderboardPage
     */
    public function execute(int $gameVersion, int $membershipType, int $page): LeaderboardPage
    {
        $result = $this->leaderboardRepository->get($gameVersion, $membershipType, $page);
        $totalPlayers = $this->leaderboardRepository->count();
        $totalPlayersPlatform = $this->leaderboardRepository->count($gameVersion, $membershipType);
        return $this->factory->getLeaderboardPage($result, $page, \count($result), $totalPlayers, $totalPlayersPlatform);
    }
}