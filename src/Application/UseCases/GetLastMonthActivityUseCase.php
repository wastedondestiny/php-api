<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use DateTime;
use WastedOnDestiny\Application\Exceptions\InvalidGameVersionException;
use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Business\Enums\DestinyGameVersion;
use WastedOnDestiny\Business\Models\Activity;
use WastedOnDestiny\Business\Repositories\Destiny2\ActivityHistoryRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny2\ProfileRepositoryInterface;

class GetLastMonthActivityUseCase implements GetLastMonthActivityUseCaseInterface
{
    /** @var ActivityHistoryRepositoryInterface */
    private $activityHistoryRepository;
    /** @var ProfileRepositoryInterface */
    private $profileRepository;
    /** @var ModelFactory */
    private $factory;

    public function __construct(
        ProfileRepositoryInterface $profileRepository,
        ActivityHistoryRepositoryInterface $activityHistoryRepository)
    {
        $this->activityHistoryRepository = $activityHistoryRepository;
        $this->profileRepository = $profileRepository;
        $this->factory = new ModelFactory();
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @return int[]
     * @throws InvalidGameVersionException
     * @throws \Exception
     */
    public function execute(int $gameVersion, int $membershipType, string $membershipId): array
    {
        switch ($gameVersion) {
            case DestinyGameVersion::Destiny:
                throw new InvalidGameVersionException('This action only supports Destiny 2 accounts');
                break;
            case DestinyGameVersion::Destiny2:
                $activities = $this->getDestiny2Activities($membershipType, $membershipId);
                break;
            default:
                throw new InvalidGameVersionException(sprintf('Unknown game version: %s', $gameVersion));
        }

        return $this->factory->getLastMonthActivity($activities);
    }

    /**
     * @param int $membershipType
     * @param string $membershipId
     * @return Activity[]
     * @throws \Exception
     */
    public function getDestiny2Activities(int $membershipType, string $membershipId): array
    {
        $profile = $this->profileRepository->get($membershipType, $membershipId, []);
        $lastMonth = new DateTime('-1 month');
        $allActivities = [];

        foreach ($profile->characters as $character) {
            $hasMore = true;
            $page = 0;

            while ($hasMore) {
                $activities = $this->activityHistoryRepository->get($membershipType, $membershipId, $character->characterId, $page);

                if (empty($activities)) {
                    break;
                }

                /** @var Activity $activity */
                foreach ($activities as $activity) {
                    if (new DateTime($activity->period) < $lastMonth) {
                        $hasMore = false;
                        break;
                    }

                    $allActivities[] = $activity;
                }

                $page++;
            }
        }

        return $allActivities;
    }
}
