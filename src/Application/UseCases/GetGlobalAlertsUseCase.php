<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Application\Models\GlobalAlert;
use WastedOnDestiny\Business\Models\GlobalAlert as BusinessGlobalAlert;
use WastedOnDestiny\Business\Repositories\Core\GlobalAlertsRepositoryInterface;

class GetGlobalAlertsUseCase implements GetGlobalAlertsUseCaseInterface
{
    /** @var GlobalAlertsRepositoryInterface */
    private $globalAlertsRepository;
    /** @var ModelFactory */
    private $factory;

    public function __construct(GlobalAlertsRepositoryInterface $globalAlertsRepository)
    {
        $this->globalAlertsRepository = $globalAlertsRepository;
        $this->factory = new ModelFactory();
    }

    /**
     * @return GlobalAlert[]
     */
    public function execute(): array
    {
        $result = $this->globalAlertsRepository->get();
        $alerts = [];

        /** @var BusinessGlobalAlert $item */
        foreach ($result as $item) {
            $alerts[] = $this->factory->getGlobalAlert($item);
        }

        return $alerts;
    }
}
