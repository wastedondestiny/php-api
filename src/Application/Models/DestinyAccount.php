<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\Models;

use DateTime;

class DestinyAccount
{
    /** @var int */
    public $membershipType;

    /** @var string */
    public $membershipId;

    /** @var int */
    public $gameVersion;

    /** @var string */
    public $currentlyPlaying;

    /** @var string */
    public $currentlyPlayingMode;

    /** @var bool */
    public $crossSaveActive;

    /** @var string */
    public $displayName;

    /** @var bool */
    public $isBungieName;

    /** @var DateTime */
    public $dateLastPlayed;

    /** @var BungieAccount|null */
    public $bungieAccount;

    /** @var int */
    public $score;

    /** @var int */
    public $legacyScore;
}
