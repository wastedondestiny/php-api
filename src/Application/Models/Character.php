<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\Models;

class Character
{
    /** @var string */
    public $backgroundPath;

    /** @var string */
    public $characterId;

    /** @var string */
    public $charClass;

    /** @var bool */
    public $deleted;

    /** @var string */
    public $emblemPath;

    /** @var int */
    public $power;

    /** @var float */
    public $pvpKda;

    /** @var float */
    public $pveKda;

    /** @var string */
    public $gender;

    /** @var int */
    public $legend;

    /** @var int */
    public $level;

    /** @var string */
    public $race;

    /** @var int */
    public $timePlayed;

    /** @var int */
    public $timeAfk;

    /** @var string */
    public $title;
}
