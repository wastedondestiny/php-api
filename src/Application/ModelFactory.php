<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application;

use DateInterval;
use DatePeriod;
use DateTime;
use WastedOnDestiny\Application\Models\AutocompletePlayer;
use WastedOnDestiny\Application\Models\Breakdown;
use WastedOnDestiny\Application\Models\BungieAccount;
use WastedOnDestiny\Application\Models\Character;
use WastedOnDestiny\Application\Models\DestinyAccount;
use WastedOnDestiny\Application\Models\DestinyVersions;
use WastedOnDestiny\Application\Models\GlobalAlert;
use WastedOnDestiny\Application\Models\LeaderboardEntry;
use WastedOnDestiny\Application\Models\LeaderboardPage;
use WastedOnDestiny\Application\Models\LeaderboardRank;
use WastedOnDestiny\Business\Enums\Destiny2Expansion;
use WastedOnDestiny\Business\Enums\Destiny2Season;
use WastedOnDestiny\Business\Enums\DestinyGameVersion;
use WastedOnDestiny\Business\Models\Account;
use WastedOnDestiny\Business\Models\Activity;
use WastedOnDestiny\Business\Models\ActivityBreakdown;
use WastedOnDestiny\Business\Models\BungieAccount as BusinessBungieAccount;
use WastedOnDestiny\Business\Models\CharacterStats;
use WastedOnDestiny\Business\Models\CurrentActivity;
use WastedOnDestiny\Business\Models\Destiny2CharacterData;
use WastedOnDestiny\Business\Models\DestinyCharacterData;
use WastedOnDestiny\Business\Models\ElasticPlayer;
use WastedOnDestiny\Business\Models\GlobalAlert as BusinessGlobalAlert;
use WastedOnDestiny\Business\Models\Membership;
use WastedOnDestiny\Business\Models\Profile;
use WastedOnDestiny\Business\Models\LeaderboardEntry as BusinessLeaderboardEntry;

class ModelFactory
{
    /**
     * @param Membership $membership
     * @param Account $account
     * @return DestinyAccount
     */
    public function getDestinyAccount(Membership $membership, Account $account): DestinyAccount
    {
        $destinyAccount = new DestinyAccount();
        $destinyAccount->membershipType = $membership->membershipType;
        $destinyAccount->membershipId = $membership->membershipId;
        $destinyAccount->displayName = $membership->displayName;
        $destinyAccount->isBungieName = false;
        $destinyAccount->bungieAccount = $membership->bungieAccount !== null ? $this->getBungieAccount($membership->bungieAccount) : null;
        $destinyAccount->gameVersion = DestinyGameVersion::Destiny;
        $destinyAccount->score = $account->grimoire;
        $destinyAccount->dateLastPlayed = $account->dateLastPlayed;
        return $destinyAccount;
    }

    /**
     * @param Membership $membership
     * @param Profile $profile
     * @param array $activityDefinitions
     * @return DestinyAccount
     */
    public function getDestiny2Account(
        Membership $membership,
        Profile $profile,
        array $activityDefinitions): DestinyAccount
    {
        $splitBungieName = explode('#', $membership->globalDisplayName);
        $isBungieDisplayName = (count($splitBungieName) > 1 && !empty($splitBungieName[0]) && !empty($splitBungieName[1]));
        $displayName = $isBungieDisplayName ? $membership->globalDisplayName : $membership->displayName;

        $destinyAccount = new DestinyAccount();
        $destinyAccount->membershipType = $membership->membershipType;
        $destinyAccount->membershipId = $membership->membershipId;
        $destinyAccount->displayName = $displayName;
        $destinyAccount->isBungieName = $isBungieDisplayName;
        $destinyAccount->bungieAccount = $membership->bungieAccount !== null ? $this->getBungieAccount($membership->bungieAccount) : null;
        $destinyAccount->gameVersion = DestinyGameVersion::Destiny2;
        $destinyAccount->score = $profile->triumph;
        $destinyAccount->legacyScore = $profile->legacyTriumph;
        $destinyAccount->crossSaveActive = $profile->crossSaveActive;
        $currentActivity = $this->getCurrentActivity($profile->characterActivities, $activityDefinitions);

        if ($currentActivity !== null) {
            $destinyAccount->currentlyPlaying = $currentActivity->currentActivity;
            $destinyAccount->currentlyPlayingMode = $currentActivity->currentActivityMode;
        } else {
            $destinyAccount->dateLastPlayed = $profile->dateLastPlayed;
        }

        return $destinyAccount;
    }

    /**
     * @param array $characterActivities
     * @param array $activityDefinitions
     * @return CurrentActivity
     */
    public function getCurrentActivity(array $characterActivities, array $activityDefinitions): ?CurrentActivity
    {
        if ($characterActivities !== null && \count($characterActivities)) {
            usort($characterActivities, static function($a, $b) {
                $ad = new DateTime($a->dateActivityStarted);
                $bd = new DateTime($b->dateActivityStarted);

                if ($ad == $bd) {
                    return 0;
                }

                return $ad < $bd ? 1 : -1;
            });
            $currentActivity = reset($characterActivities);

            if ($currentActivity !== null && $currentActivity->currentActivityHash !== null && $currentActivity->currentActivityHash !== 0) {
                $activity = $activityDefinitions[$currentActivity->currentActivityHash];

                if (!isset($activity['name']) || empty($activity['name'])) {
                    return new CurrentActivity('In orbit');
                }

                return new CurrentActivity(
                    $activity['name'],
                    $activity['mode']['name'] ?? ''
                );
            }
        }

        return null;
    }

    /**
     * @param BusinessBungieAccount $bungieAccount
     * @return BungieAccount
     */
    public function getBungieAccount(BusinessBungieAccount $bungieAccount): BungieAccount
    {
        $bungieNetAccount = new BungieAccount();
        $bungieNetAccount->displayName = $bungieAccount->displayName;
        $bungieNetAccount->membershipId = $bungieAccount->membershipId;
        $bungieNetAccount->firstAccess = $bungieAccount->firstAccess;
        return $bungieNetAccount;
    }

    /**
     * @param CharacterStats $stats
     * @param DestinyCharacterData|null $data
     * @return Character
     */
    public function getDestinyCharacter(CharacterStats $stats, ?DestinyCharacterData $data): Character
    {
        $character = new Character();
        $character->characterId = $stats->characterId;
        $character->deleted = $stats->deleted;
        $character->pvpKda = $stats->pvpKda;
        $character->pveKda = $stats->pveKda;
        $character->timePlayed = $stats->timePlayed;

        if ($data !== null) {
            $character->race = $data->race;
            $character->gender = $data->gender;
            $character->charClass = $data->charClass;
            $character->emblemPath = $data->emblemPath;
            $character->backgroundPath = $data->backgroundPath;
            $character->power = $data->light;
            $character->level = $data->level;
        }

        $character->timeAfk = 0;
        $character->legend = 0;
        return $character;
    }

    /**
     * @param CharacterStats $stats
     * @param string[] $seals
     * @param Destiny2CharacterData|null $data
     * @return Character
     */
    public function getDestiny2Character(CharacterStats $stats, array $seals, ?Destiny2CharacterData $data): Character
    {
        $character = new Character();
        $character->characterId = $stats->characterId;
        $character->deleted = $stats->deleted;
        $character->pvpKda = $stats->pvpKda;
        $character->pveKda = $stats->pveKda;
        $character->timePlayed = $stats->timePlayed;

        if ($data !== null) {
            $character->race = $data->race;
            $character->gender = $data->gender;
            $character->charClass = $data->charClass;
            $character->emblemPath = $data->emblemPath;
            $character->backgroundPath = $data->backgroundPath;
            $character->power = $data->power;
            $character->level = $data->level;
            $character->legend = $data->legend;
            $character->title = $seals[$data->title];
            $character->timeAfk = $data->timeAfk - $stats->timePlayed;
        }

        return $character;
    }

    /**
     * @param BusinessGlobalAlert $alertItem
     * @return GlobalAlert
     */
    public function getGlobalAlert(BusinessGlobalAlert $alertItem): GlobalAlert
    {
        $alert = new GlobalAlert();
        $alert->html = $alertItem->html;
        $alert->timestamp = $alertItem->timestamp;
        $alert->link = $alertItem->link;
        $alert->level = $alertItem->level;
        $alert->type = $alertItem->type;
        return $alert;
    }

    /**
     * @param Activity[] $activities
     * @return array
     * @throws \Exception
     */
    public function getLastMonthActivity(array $activities): array
    {
        $lastMonth = new DateTime('-1 month');
        $now = new DateTime('now');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($lastMonth, $interval, $now);
        $lastMonthActivity = [];

        /** @var DateTime $day */
        foreach ($period as $day) {
            $lastMonthActivity[$day->format('Y-m-d')] = 0;

            /** @var Activity $activity */
            foreach ($activities as $activity) {
                if ((new DateTime($activity->period))->format('Y-m-d') === $day->format('Y-m-d')) {
                    $lastMonthActivity[$day->format('Y-m-d')] += $activity->timePlayed;
                }
            }
        }

        return $lastMonthActivity;
    }

    /**
     * @param BusinessLeaderboardEntry[] $entries
     * @param int $page
     * @param int $count
     * @param int $totalPlayers
     * @param int $totalPlayersPlatform
     * @return LeaderboardPage
     */
    public function getLeaderboardPage(array $entries, int $page, int $count, int $totalPlayers, int $totalPlayersPlatform): LeaderboardPage
    {
        $players = [];

        foreach ($entries as $entry) {
            $players[] = $this->getLeaderboardEntry($entry);
        }

        $leaderboardPage = new LeaderboardPage();
        $leaderboardPage->players = $players;
        $leaderboardPage->page = $page;
        $leaderboardPage->count = $count;
        $leaderboardPage->totalPlayers = $totalPlayers;
        $leaderboardPage->totalPlayersPlatform = $totalPlayersPlatform;
        return $leaderboardPage;
    }

    /**
     * @param BusinessLeaderboardEntry $entry
     * @return LeaderboardEntry
     */
    public function getLeaderboardEntry(BusinessLeaderboardEntry $entry): LeaderboardEntry
    {
        $leaderboardEntry = new LeaderboardEntry();
        $leaderboardEntry->gameVersion = $entry->gameVersion;
        $leaderboardEntry->membershipType = $entry->membershipType;
        $leaderboardEntry->timePlayed = $entry->timePlayed;
        $leaderboardEntry->membershipId = $entry->membershipId;
        $leaderboardEntry->displayName = $entry->displayName;
        return $leaderboardEntry;
    }

    /**
     * @param int $value
     * @param bool $isTop
     * @return LeaderboardRank
     */
    public function getLeaderboardRank(int $value, bool $isTop = false): LeaderboardRank
    {
        $leaderboardRank = new LeaderboardRank();
        $leaderboardRank->value = $value;
        $leaderboardRank->isTop = $isTop;
        return $leaderboardRank;
    }

    /**
     * @param ActivityBreakdown $activityBreakdown
     * @return Breakdown
     */
    public function getActivityBreakdown(ActivityBreakdown $activityBreakdown): Breakdown
    {
        $breakdown = new Breakdown();
        $breakdown->story = $activityBreakdown->story;
        $breakdown->strikes = $activityBreakdown->strike;
        $breakdown->raid = $activityBreakdown->raid;
        $breakdown->gambit = $activityBreakdown->gambit;
        $breakdown->crucible = $activityBreakdown->crucible;
        $breakdown->total = $breakdown->story + $breakdown->strikes + $breakdown->raid + $breakdown->gambit + $breakdown->crucible;
        $breakdown->empty = $breakdown->total === 0;
        return $breakdown;
    }

    /**
     * @param ElasticPlayer $entry
     * @return AutocompletePlayer
     */
    public function getAutocompletePlayer(ElasticPlayer $entry): AutocompletePlayer
    {
        $player = new AutocompletePlayer();

        $player->displayName = $entry->displayName;
        $player->membershipId = $entry->membershipId;
        $player->membershipType = $entry->membershipType;

        return $player;
    }
}
