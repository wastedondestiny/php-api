<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess;

interface DatabaseContextInterface
{
    public function insert(string $table, array $data): void;
    public function update(string $table, array $data, array $where = []): void;
    public function replace(string $table, array $data, array $where = []): void;
    public function select(string $table, array $columns, array $where = []): array;
    public function query(string $request, array $data = []): void;
    public function fetchQuery(string $request, array $data = []): array;
}