<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Enums;

class DestinyComponentType
{
    public const None = 0;
    public const Profiles = 100;
    public const VendorReceipts = 101;
    public const ProfileInventories = 102;
    public const ProfileCurrencies = 103;
    public const ProfileProgression = 104;
    public const PlatformSilver = 105;
    public const Characters = 200;
    public const CharacterInventories = 201;
    public const CharacterProgressions = 202;
    public const CharacterRenderData = 203;
    public const CharacterActivities = 204;
    public const CharacterEquipment = 205;
    public const ItemInstances = 300;
    public const ItemObjectives = 301;
    public const ItemPerks = 302;
    public const ItemRenderData = 303;
    public const ItemStats = 304;
    public const ItemSockets = 305;
    public const ItemTalentGrids = 306;
    public const ItemCommonData = 307;
    public const ItemPlugStates = 308;
    public const ItemPlugObjectives = 308;
    public const ItemReusablePlugs = 308;
    public const Vendors = 400;
    public const VendorCategories = 401;
    public const VendorSales = 402;
    public const Kiosks = 500;
    public const CurrencyLookups = 600;
    public const PresentationNodes = 700;
    public const Collectibles = 800;
    public const Records = 900;
    public const Transitory = 1000;
    public const Metrics = 1100;
}
