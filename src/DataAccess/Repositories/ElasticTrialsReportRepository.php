<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories;

use WastedOnDestiny\DataAccess\GuzzleContextInterface;

abstract class ElasticTrialsReportRepository extends Repository
{
    /** @var GuzzleContextInterface */
    private $context;

    protected const BASE_URL = 'https://elastic.destinytrialsreport.com';

    public function __construct(GuzzleContextInterface $context) {
        parent::__construct();
        $this->context = $context;
    }

    /**
     * @return GuzzleContextInterface
     */
    public function getContext(): GuzzleContextInterface {
        return $this->context;
    }
}
