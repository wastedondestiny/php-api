<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Players;

use WastedOnDestiny\Business\Models\ElasticPlayer;
use WastedOnDestiny\Business\Repositories\Players\SearchRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\ElasticTrialsReportRepository;

final class SearchRepository extends ElasticTrialsReportRepository implements SearchRepositoryInterface
{
    /**
     * @param int $membershipType
     * @param string $displayName
     * @return ElasticPlayer[]
     */
    public function get(int $membershipType, string $displayName): array
    {
        $response = $this->getContext()->call(
            'GET',
            vsprintf(
                self::BASE_URL . '/players/%s/%s',
                [ $membershipType, $displayName ]
            )
        );

        if (!empty($response)) {
            return array_map(function ($item) {
                return $this->getFactory()->getElasticPlayer($item);
            }, json_decode($response, true));
        } else {
            return [];
        }
    }
}
