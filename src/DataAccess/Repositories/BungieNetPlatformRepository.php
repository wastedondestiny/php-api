<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories;

use WastedOnDestiny\DataAccess\BungieNetPlatformContextInterface;

abstract class BungieNetPlatformRepository extends Repository
{
    /** @var BungieNetPlatformContextInterface */
    private $context;

    protected const BASE_URL = 'https://www.bungie.net';

    public function __construct(BungieNetPlatformContextInterface $context) {
        parent::__construct();
        $this->context = $context;
    }

    /**
     * @return BungieNetPlatformContextInterface
     */
    public function getContext(): BungieNetPlatformContextInterface {
        return $this->context;
    }
}
