<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Leaderboard;

use WastedOnDestiny\Business\Models\LeaderboardEntry;
use WastedOnDestiny\Business\Repositories\Leaderboard\LeaderboardRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\DatabaseRepository;

final class LeaderboardRepository extends DatabaseRepository implements LeaderboardRepositoryInterface
{
    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param int $page
     * @return LeaderboardEntry[]
     */
    public function get(int $gameVersion, int $membershipType, int $page): array
    {
        if ($membershipType > 0) {
            $result = $this->getContext()->select('leaderboard', [
                'membershipId',
                'membershipType',
                'displayName',
                'timePlayed',
                'gameVersion'
            ], [
                'membershipType' => $membershipType,
                'gameVersion' => $gameVersion,
                'ORDER' => [ 'timePlayed' => 'DESC' ],
                'LIMIT' => [ ($page - 1) * 10, 10 ]
            ]);
        } else {
            $result = $this->getContext()->select('leaderboard', [
                'membershipId',
                'membershipType',
                'displayName',
                'timePlayed',
                'gameVersion'
            ], [
                'gameVersion' => $gameVersion,
                'ORDER' => [ 'timePlayed' => 'DESC' ],
                'LIMIT' => [ ($page - 1) * 10, 10 ]
            ]);
        }

        return $this->getFactory()->getLeaderboardEntries($result);
    }

    /**
     * @param int|null $gameVersion
     * @param int|null $membershipType
     * @return int
     */
    public function count(int $gameVersion = null, int $membershipType = null): int
    {
        if ($gameVersion > 0 && $membershipId > 0) {
            [ $result ] = $this->getContext()->fetchQuery(
                "SELECT COUNT(`membershipId`) AS `count` FROM `leaderboard` WHERE `gameVersion` = $gameVersion && `membershipType` = $membershipType");
        } else if ($gameVersion > 0) {
            [ $result ] = $this->getContext()->fetchQuery(
                "SELECT COUNT(`membershipId`) AS `count` FROM `leaderboard` WHERE `gameVersion` = $gameVersion");
        } else {
            [ $result ] = $this->getContext()->fetchQuery('SELECT COUNT(`membershipId`) AS `count` FROM `leaderboard`');
        }

        return (int)$result[0];
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @return LeaderboardEntry|null
     */
    public function getSingle(int $gameVersion, int $membershipType, string $membershipId): ?LeaderboardEntry
    {
        if ($membershipType > 0) {
            $result = $this->getContext()->select('leaderboard', [
                'membershipId',
                'membershipType',
                'displayName',
                'timePlayed',
                'gameVersion'
            ], [
                'membershipId' => $membershipId,
                'membershipType' => $membershipType,
                'gameVersion' => $gameVersion
            ]);
        } else {
            $result = $this->getContext()->select('leaderboard', [
                'membershipId',
                'membershipType',
                'displayName',
                'timePlayed',
                'gameVersion'
            ], [
                'membershipId' => $membershipId,
                'gameVersion' => $gameVersion
            ]);
        }

        if (!\count($result)) {
            return null;
        }

        return $this->getFactory()->getLeaderboardEntries($result)[0];
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @param int $timePlayed
     * @param string $displayName
     */
    public function insert(int $gameVersion, int $membershipType, string $membershipId, int $timePlayed, string $displayName): void
    {
        $this->getContext()->insert('leaderboard', [
            'gameVersion' => $gameVersion,
            'membershipType' => $membershipType,
            'membershipId' => $membershipId,
            'timePlayed' => $timePlayed,
            'displayName' => $displayName
        ]);
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @param int $timePlayed
     * @param string $displayName
     */
    public function upsert(int $gameVersion, int $membershipType, string $membershipId, int $timePlayed, string $displayName): void
    {
        $this->getContext()->query("INSERT INTO leaderboard (membershipId, displayName, membershipType, timePlayed, gameVersion)
                                    VALUES (:membershipId, :displayName, $membershipType, $timePlayed, $gameVersion)
                                    ON DUPLICATE KEY UPDATE displayName=:displayName, timePlayed=$timePlayed;", [
                                        ':membershipId' => $membershipId,
                                        ':displayName' => $displayName
                                    ]);
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @param int $timePlayed
     * @param string $displayName
     */
    public function update(int $gameVersion, int $membershipType, string $membershipId, int $timePlayed, string $displayName): void
    {
        $this->getContext()->update('leaderboard', [
            'timePlayed' => $timePlayed,
            'displayName' => $displayName
        ], [
            'gameVersion' => $gameVersion,
            'membershipType' => $membershipType,
            'membershipId' => $membershipId,
        ]);
    }
}
