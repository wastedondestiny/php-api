<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Leaderboard;

use WastedOnDestiny\Business\Repositories\Leaderboard\PercentileRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\DatabaseRepository;

final class PercentileRepository extends DatabaseRepository implements PercentileRepositoryInterface
{
    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @return int[]
     */
    public function get(int $gameVersion, int $membershipType): array
    {
        if ($membershipType > 0) {
            $result = $this->getContext()->select('percentiles', [
                'rank',
                'timePlayed'
            ], [
                'membershipType' => $membershipType,
                'gameVersion' => $gameVersion,
                'ORDER' => [ 'rank' => 'ASC' ]
            ]);
        } else {
            $result = $this->getContext()->select('percentiles', [
                'rank',
                'timePlayed'
            ], [
                'gameVersion' => $gameVersion,
                'ORDER' => [ 'rank' => 'ASC' ]
            ]);
        }

        return $this->getFactory()->getPercentiles($result);
    }
}
