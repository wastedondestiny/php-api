<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Destiny;

use WastedOnDestiny\Business\Models\CharacterStats;
use WastedOnDestiny\Business\Repositories\Destiny\AccountHistoricalStatsRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class AccountHistoricalStatsRepository extends BungieNetPlatformRepository implements AccountHistoricalStatsRepositoryInterface
{
    /**
     * @param int $membershipType
     * @param string $membershipId
     * @return CharacterStats[]
     */
    public function get(int $membershipType, string $membershipId): array
    {
        $result = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/d1/Platform/Destiny/Stats/Account/%s/%s/',
                [ $membershipType, $membershipId ]
            )
        );

        return $this->getFactory()->getCharacterStats($result);
    }
}
