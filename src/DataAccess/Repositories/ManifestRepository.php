<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories;

use WastedOnDestiny\DataAccess\FileContextInterface;

abstract class ManifestRepository extends Repository
{
    /** @var FileContextInterface */
    private $context;

    public function __construct(FileContextInterface $context) {
        parent::__construct();
        $this->context = $context;
    }

    /**
     * @return FileContextInterface
     */
    public function getContext(): FileContextInterface {
         return $this->context;
    }
}
