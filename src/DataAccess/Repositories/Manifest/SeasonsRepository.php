<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Manifest;

use WastedOnDestiny\Business\Repositories\Manifest\SeasonsRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\ManifestRepository;

final class SeasonsRepository extends ManifestRepository implements SeasonsRepositoryInterface
{
    /**
     * @return array
     */
    public function get(): array
    {
        return $this->getContext()->make('./manifest/seasons.json');
    }
}
