<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Destiny2;

use WastedOnDestiny\Business\Models\DestinyPlayer;
use WastedOnDestiny\Business\Repositories\Destiny2\SearchDestinyPlayerByBungieNameRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class SearchDestinyPlayerByBungieNameRepository extends BungieNetPlatformRepository implements SearchDestinyPlayerByBungieNameRepositoryInterface
{
    /**
     * @param string $displayName
     * @return DestinyPlayer[]
     */
    public function get(string $displayName): array
    {
        $splitName = explode('#', $displayName);
        $result = $this->getContext()->make(
            'POST',
            self::BASE_URL . '/Platform/Destiny2/SearchDestinyPlayerByBungieName/All/',
            [
                "displayName" => $splitName[0],
                "displayNameCode" => count($splitName) > 1 ? intval($splitName[1], 10) : 0
            ]
        );

        return $this->getFactory()->getDestinyPlayers($result);
    }
}
