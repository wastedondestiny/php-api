<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Destiny2;

use WastedOnDestiny\Business\Models\Activity;
use WastedOnDestiny\Business\Repositories\Destiny2\ActivityHistoryRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class ActivityHistoryRepository extends BungieNetPlatformRepository implements ActivityHistoryRepositoryInterface
{
    /**
     * @param int $membershipType
     * @param string $membershipId
     * @param string $characterId
     * @param int $page
     * @return Activity[]|null
     */
    public function get(int $membershipType, string $membershipId, string $characterId, int $page): ?array
    {
        $result = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/Platform/Destiny2/%s/Account/%s/Character/%s/Stats/Activities/',
                [ $membershipType, $membershipId, $characterId ]
            ),
            [ 'mode' => 'None', 'count' => 250, 'page' => $page ]
        );

        return $this->getFactory()->getActivities($result);
    }
}
