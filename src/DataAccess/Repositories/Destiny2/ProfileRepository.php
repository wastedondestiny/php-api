<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Destiny2;

use WastedOnDestiny\Business\Models\Profile;
use WastedOnDestiny\Business\Repositories\Destiny2\ProfileRepositoryInterface;
use WastedOnDestiny\DataAccess\BungieNetPlatformContextInterface;
use WastedOnDestiny\DataAccess\Enums\DestinyComponentType;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class ProfileRepository extends BungieNetPlatformRepository implements ProfileRepositoryInterface
{
    public function __construct(BungieNetPlatformContextInterface $context) {
        parent::__construct($context);
    }

    /**
     * @param int $membershipType
     * @param string $membershipId
     * @param array $seasonPasses
     * @return Profile|null
     */
    public function get(int $membershipType, string $membershipId, array $seasonPasses)
    {
        $components = [
            DestinyComponentType::Profiles,
            DestinyComponentType::ProfileProgression,
            DestinyComponentType::Characters,
            DestinyComponentType::CharacterProgressions,
            DestinyComponentType::CharacterActivities,
            DestinyComponentType::Records
        ];

        $result = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/Platform/Destiny2/%s/Profile/%s/',
                [ $membershipType, $membershipId ]
            ),
            [ 'components' => implode(',', $components) ]
        );

        if (!empty($result->message)) {
            return $this->getFactory()->getProfile($result, $seasonPasses);
        } else {
            return null;
        }
    }
}
