<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\User;

use WastedOnDestiny\Business\Models\Membership;
use WastedOnDestiny\Business\Repositories\User\MembershipRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class MembershipRepository extends BungieNetPlatformRepository implements MembershipRepositoryInterface
{
    /**
     * @param int $membershipType
     * @param string $membershipId
     * @return Membership[]
     */
    public function get(int $membershipType, string $membershipId): array
    {
        $response = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/Platform/User/GetMembershipsById/%s/%s/',
                [ $membershipId, $membershipType ]
            )
        );

        return $this->getFactory()->getMemberships($response);
    }
}
